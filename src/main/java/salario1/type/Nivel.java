package salario1.type;

public enum Nivel {

    JUNIOR("Junior", 2.1), PLENO("Pleno", 3.3), SENIOR("Sênior", 5.0),
    ESPECIALISTA("Especialista", 7.2), GERENTE("Gerente", 9.6);

    String nomeNivel;
    double fatorDeMultiplicacao;

    Nivel(String nomeNivel, double fatorDeMultiplicacao) {
        this.nomeNivel = nomeNivel;
        this.fatorDeMultiplicacao = fatorDeMultiplicacao;
    }

    public String getNivel() {
        return nomeNivel;
    }

    public double getFatorDeMultiplicacao() {
        return  fatorDeMultiplicacao;
    }

    @Override
    public String toString() {
        return "Nivel{" +
                "nomeNivel='" + nomeNivel + '\'' +
                ", fatorDeMultiplicacao=" + fatorDeMultiplicacao +
                '}';
    }
}
