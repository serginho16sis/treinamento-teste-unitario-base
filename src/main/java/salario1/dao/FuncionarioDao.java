package salario1.dao;

import salario1.domain.Funcionario;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FuncionarioDao {

    private List<Funcionario> funcionarioList;
    private int sequencia = 1000;

    public FuncionarioDao() {
        funcionarioList = new ArrayList<Funcionario>();
    }

    public FuncionarioDao(List<Funcionario> funcionarioList) {
        this.funcionarioList = funcionarioList;
    }

    public void setFuncionarioList(List<Funcionario> funcionarioList) {
        this.funcionarioList = funcionarioList;
    }

    public List<Funcionario> getFuncionarioList() {
        return funcionarioList;
    }

    public void create(Funcionario funcionario) {
        if (funcionario.getId() != null) {
            throw new RuntimeException("Funcionario ja existente.");
        }
        funcionario.setId(sequencia++);
        funcionarioList.add(funcionario);
    }

    public void delete(Funcionario funcionario) {
        Iterator<Funcionario> iterator = funcionarioList.listIterator();
        boolean funcionarioEncontrado = false;
        while (iterator.hasNext()) {
            if (iterator.next().getId().equals(funcionario.getId())) {
                iterator.remove();
                funcionarioEncontrado = true;
                break;
            }
        }
    }

    public void update(Funcionario funcionario) {
        Iterator<Funcionario> iterator = funcionarioList.listIterator();
        boolean funcionarioEncontrado = false;

        while (iterator.hasNext()) {
            Funcionario dbFuncionario = iterator.next();
            if (dbFuncionario.getId().equals(funcionario.getId())) {
                dbFuncionario.setArea(funcionario.getArea());
                dbFuncionario.setNome(funcionario.getNome());
                dbFuncionario.setNivel(funcionario.getNivel());
                dbFuncionario.setSalarioMensal(funcionario.getSalarioMensal());
                funcionarioEncontrado = true;
                break;
            }
        }

        if (!funcionarioEncontrado) {
            throw new RuntimeException("Funcionario nao encontrado");
        }
    }

    public Funcionario findById(Integer id) {
        Iterator<Funcionario> iterator = funcionarioList.listIterator();

        while (iterator.hasNext()) {
            Funcionario dbFuncionario = iterator.next();
            if (dbFuncionario.getId().equals(id)) {
                return dbFuncionario;
            }
        }

        throw new RuntimeException("Funcionario not found.");
    }
}
